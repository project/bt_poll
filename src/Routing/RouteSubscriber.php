<?php

namespace Drupal\bt_poll\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    $collection->remove(['poll.poll_list']);

    // Change path '/poll/add' to '/app/websiteb/poll/create'.
    if ($route = $collection->get('poll.poll_add')) {
      $route->setPath('/app/website/polls/create');
    }
  }

}
