<?php

namespace Drupal\bt_poll\Form;

use Drupal\poll\Form\PollForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the poll edit forms.
 */
class OverridePollForm extends PollForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $poll = $this->entity;
    $insert = (bool) $poll->id();
    $poll->save();
    if ($insert) {
      drupal_set_message($this->t('The poll %poll has been updated.', array('%poll' => $poll->label())));
    }
    else {
      \Drupal::logger('poll')->notice('Poll %poll added.', array('%poll' => $poll->label(), 'link' => $poll->link($poll->label())));
      drupal_set_message($this->t('The poll %poll has been added.', array('%poll' => $poll->label())));
    }

    $form_state->setRedirect('page_manager.page_view_app_website_polls_app_website_polls-panels_variant-0');
  }

}
