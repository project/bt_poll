<?php

namespace Drupal\bt_poll\Form;

use Drupal\poll\Form\PollDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting a poll.
 */
class OverridePollDeleteForm extends PollDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    \Drupal::logger('poll')->notice('Poll %poll deleted.', array('%poll' => $this->entity->label()));
    drupal_set_message($this->t('The poll %poll has been deleted.', array('%poll' => $this->entity->label())));
    $form_state->setRedirect('page_manager.page_view_app_website_polls_app_website_polls-panels_variant-0');
  }

}
