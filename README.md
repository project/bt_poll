This module adapts the functions of the Poll module to follow the navigability of Btester CMS.

Make the following changes:

* Remove the route poll.poll_list
* Change the URL of poll.poll_add route from /poll/add to /app/website/polls/create
* Change local actions poll.poll_add to appears on /app/website/polls
* Unset poll.poll_list local task
* Override edit|default|delete forms for redirect to /app/website/polls

Adds the following settings:

* Add panel page /app/website/polls
* Add administrative views to list all polls
* Add a link to /app/website/polls in bt-menu-app menu
* Add a link to /app/website/polls/create in bt-menu-app menu
* Add a local task to /app/website/polls under /app/website

Other:

* Add breadcrumbs to poll.poll_add and entity.poll.edit_form routes. ($site_name > Website > Polls)


This module is part of an ecosystem of modules to start building applications and it depends on Btester CMS.
